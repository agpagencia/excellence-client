<?php
/**
 * Main Excellence_Custom_Admin_Login Class
 *
 * @class Excellence_Custom_Admin_Login
 * @version	0.0.2
 * @package	Excellence_Custom_Admin_Login
 */

class Excellence_Custom_Admin_Login {
    /**
	 * Instance of this class.
	 *
	 * @var object
	 */
	protected static $instance = null;

	/**
	 * Set up the plugin
	 */
	public function __construct() {

		if ( $GLOBALS['pagenow'] == 'wp-login.php' ) {
			add_action( 'login_init', array( $this, 'excellence_login_css' ) );
			add_filter( 'login_headerurl', array( $this, 'excellence_login_headerurl' ) );
			add_filter( 'login_headertext', array( $this, 'excellence_login_headertext' ) );
			add_action( 'login_footer', array( $this, 'excellence_message_login' ) );
			add_filter( 'login_errors', array( $this, 'excellence_login_errors' ) );
		}
		
	}

	/**
	 * Return the plugin instance.
	 */
	public static function init() {
		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	/**
	 * 
	 * Enqueue the login CSS
	 * 
	 */
	public function excellence_login_css() {
		wp_enqueue_style( 'excellence-login-page',  plugins_url( '/assets/css/excellence-client-login-page.css', dirname( __DIR__ ) ) );
	}

	/**
	 * 
	 * Changing the logo link from wordpress.org to AGP Agência
	 * 
	 */
	function excellence_login_headerurl() { 
        return 'https://agpagencia.com.br/';
	}
	
	/**
	 * 
	 * Changing the alt text on the logo to show AGP Agência de Comunicação
	 * 
	 */
	function excellence_login_headertext( $headertext ) {

		$headertext = esc_html__( 'AGP Agência de Comunicação', 'excellence' );
	    return $headertext;

    }
    
	/**
	 * 
	 * Print the message to help users in Login page
	 * 
	 */
	public function excellence_message_login() {
		echo '<div class="help-from-agp">Está com problemas ou dúvidas sobre seu site? Entre em contato com nossa equipe Web da <a href="https://agpagencia.com.br"><strong>AGP Agência</strong></a>.<br><br> Nos envie uma mensagem pelo <a href="https://api.whatsapp.com/send?phone=5511940138315&text=Ol%C3%A1%20AGP%20Ag%C3%AAncia.%20Preciso%20de%20ajuda%20com%20o%20meu%20site!">WhatsApp <b>(11) 9 4013 8315</a></b> ou envie um e-mail <a style="text-decoration: none;" href="mailto:' . antispambot( 'web@agpagencia.com.br' ) . '">clicando aqui!</a></div>';
	}

	/**
	 * 
	 * Change errors on login page
	 * 
	 */
	public function excellence_login_errors() {
		$message = esc_html__( 'Alguma coisa deu errado, tente novamente ou contacte o administrador do site!', 'excellence' );
		return $message;
	}	

} // End Class