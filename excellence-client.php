<?php
/**
 * Plugin Name:       Excellence Client
 * Description:       Plugin to customize the client's website using the AGP Agency's Excellence Theme (for WordPres).
 * Plugin URI:        https://gitlab.com/agpagencia/excellence-client
 * Version:           1.0.0
 * Author:            Everaldo Matias
 * Author URI:        https://everaldo.dev/
 * Requires at least: 3.0.0
 * Tested up to:      4.4.2
 *
 * @package ExcellenceClient
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Main ExcellenceClient Class
 *
 * @class ExcellenceClient
 * @version	0.0.1
 * @since 0.0.1
 * @package	ExcellenceClient
 */
class ExcellenceClient {

	/**
	 * Instance of this class.
	 *
	 * @var object
	 */
	protected static $instance = null;

	/**
	 * Plugin directory path
	 *
	 * @var string
	 */
	private $plugin_dir = null;

	/**
	 * Set up the plugin.
	 */
	function __construct() {
		
		$this->plugin_dir = plugin_dir_path( __FILE__ );
		add_action( 'init', array( $this, 'includes' ), 0 );
		
		add_action( 'init', array( 'Excellence_Custom_Admin_Login', 'init' ) );

		if ( is_admin() ) {
			add_action( 'admin_enqueue_scripts', array( $this, 'excellence_admin_css' ) );
		}

	}

	/**
	 * Return the plugin instance.
	 */
	public static function init() {
		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	/**
	 * Enqueue the CSS to Admin
	 *
	 * @return void
	 */
	public function excellence_admin_css() {
		wp_enqueue_style( 'excellence-admin-page',  plugin_dir_url( __FILE__ ) . 'assets/css/excellence-admin-page.css' );
	}

	public function includes() {
		
		// Custom Login Page
		require_once $this->plugin_dir . '/includes/classes/class-custom-admin-login.php';

		// Load the functions.php
		require_once $this->plugin_dir . '/functions.php';

	}

} // End Class

/**
* Initialize the plugin actions.
*/
add_action( 'plugins_loaded', array( 'ExcellenceClient', 'init' ) );